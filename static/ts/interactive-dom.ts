interface ajaxInterface {
  url: string,
  method: string,
  success: Function,
  data?: any
}

const _fn = {
  /**
   * Create a loop depending on NodeList object, and make a specific functionality by element.
   * @param elements Elements to execute the loop
   * @param callback Callback anonymous function to make by each element. It will send first parameter with the current element in loop.
   */
  __loopOfElements(elements: NodeList, callback: Function): void {
    for (let i = 0; i < elements.length; i++) {
      const element: Node = elements[i];
      callback(element)
    }
  },

  /**
   * Create a event with list of elements.
   * @param event The event To emit
   * @param elements List of elements to make the loop
   * @param callback The function to execute when emit the event
   */
  __loopOfElementsOnEvent(event: string, elements: NodeList, callback: Function): void {
    this.__loopOfElements(elements, function (element) {
      element.addEventListener(event, function (e) {
        callback(e, this)
      })
    })
  },
  __ajax(options:ajaxInterface){
    let xhttp = new XMLHttpRequest()
    xhttp.open(options.method, options.url, true)
    xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

    if (typeof options.data == 'undefined') {
      xhttp.send()
    } else {
      console.log(JSON.stringify(options.data))
      xhttp.send(JSON.stringify(options.data))
    }
    xhttp.onreadystatechange = function(){
      if (xhttp.readyState === xhttp.DONE ){
        if ( xhttp.status == 200 ){
          options.success(xhttp.responseText)
        } else if (xhttp.readyState == 400 ){
          alert('Error exists number 400')
        } else {
          alert('Internal server error, unknown')
        }
      }
    }
    
    
  },
  __hasClass(elem,className){
    return (' ' + elem.className + ' ').indexOf(' ' + className + ' ') > -1;
  }


}

class single_dom {

  activeObject = null;
  _fn = _fn;
  /** Verificar si se obtiene un solo elemento. */
  isSingle: boolean = false

  constructor(elementCursor) {
    if (elementCursor instanceof HTMLElement) {
      this.activeObject = [elementCursor]
    } else if (document.querySelector(elementCursor)) {
      this.activeObject = document.querySelectorAll(elementCursor)
    }

    if (this.exists() && this.activeObject.length == 1 ){
      this.isSingle = true
    }
  }


  /**
   * Check if the element had 
   * @param className ClassName to check the element
   */
  hasClass(className): boolean {
    if (this.exists()) {
      let checked: boolean = false;
      return this._fn.__hasClass(this.activeObject[0], className)
    }
  }

  /**
   * Check if the active element exists.
   * This check, can be used in events listeners, and evit the conflicts and errors.
   */
  exists(): boolean {
    if (this.activeObject) {
      return this.activeObject.length > 0
    } else {
      return false;
    }
  }

  val(): string | boolean {
    if (this.exists()) {
      return this.activeObject[0].value
    } else {
      return false
    }
  }

  append(element: string) {
    if (this.exists()) {
      _fn.__loopOfElements(this.activeObject, (elm) => {
        elm.innerHTML = elm.innerHTML + element
      })

    }
  }

  /**
  * Remove the active element.
  */
  remove(): void {
    if (this.exists()) {
      if (this.isSingle ){
        this.activeObject[0].parentNode.removeChild(this.activeObject[0])
      }else{
        this._fn.__loopOfElements(this.activeObject, function(element){
          element.parentNode.removeChild(element)
        });
      }
    }
  }

  /**
   * Agregar una clase a el objeto activo.
   * @param newClassName Nueva clase a ser agregada
   */
  addClass(newClassName: string): void {
    if (this.exists()) {
      this._fn.__loopOfElements(this.activeObject, function(elm) {
        elm.classList.add(newClassName)
      })
    }
  }

  /**
   * Crear interruptor de clase.
   * @param toggleClassName Clase que va a ser alterada
   */
  toggleClass(toggleClassName: string) {
    if (this.exists()) {
      this._fn.__loopOfElements(this.activeObject, function(elm) {
        elm.classList.toggle(toggleClassName)
      })
    }
  }

  removeClass(removeClassName: string) {
    if (this.exists()) {
      this._fn.__loopOfElements(this.activeObject, function(elm) {
        elm.classList.toggle(removeClassName)
      })
    }
  }

  /**
   * What's going on when click on the active element.
   * @param callback Functionality when click the active element. This parameter, send event as first parameter, send actual element as second parameter.
   */
  click(callback: Function): void {
    if (this.exists()) {
      this._fn.__loopOfElementsOnEvent("click", this.activeObject, function(e, actualElem) {
        callback(e, actualElem)
      })
    }
  }

  children(selector = '') {
    if (this.activeObject.length === 1) {
      let match = this.activeObject[0];
      let matchs = []
      this._fn.__loopOfElements(match.childNodes, (elm) => {
        if (elm.nodeType === 1) {
          matchs.push(elm)
        }
      })
      this.activeObject = matchs;
    } else {
      let match = []
      for (let i = 0; i < this.activeObject.length; i++) {
        let element = this.activeObject[i];
        this._fn.__loopOfElements(element.childNodes, (elem) => {
          if (selector != "") {
            if (this._fn.__hasClass(elem, selector)) {
              match.push(elem)
            }
          } else {
            if (elem.nodeType === 1) {
              match.push(elem)
            }
          }
        })

      }
      this.activeObject = match;
    }
    return this;
  }

}
function sd(selector){
  return new single_dom(selector);
}