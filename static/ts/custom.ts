window.onload = function () {
  let modalAddTag = sd("#modal_add_tag")
  let buttonModalTag = sd("#toggle_modal_tag")


  buttonModalTag.click(function(){
    modalAddTag.addClass("show")
  });

  sd(".toggle_access").click((e,elmn) =>{
    e.preventDefault();
    sd(".register_col_wrap").toggleClass("moved");
  })  

  let newTagButton = sd("#action_new_tag")
  newTagButton.click(function(event, thisElement){
    event.preventDefault();
    let titleTag = sd("#new_title_tag")
    _fn.__ajax({
      url: '/user/create_tag?new_title_tag=' + titleTag.val() ,
      method: 'GET',
      success: function(data){
        let nwData = JSON.parse(data)
        if ( nwData["success"]){
          sd("#create_post_tag").activeObject[0].disabled =false
          sd("#create_post_tag").append("<option>" + nwData["new_data"]['data_post_title'] + "</option>")
          modalAddTag.removeClass("show")
          sd("#option_no_tags").remove()
        }else{
          
        }
      }
    })
  })
}