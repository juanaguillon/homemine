window.onload = function () {
    var modalAddTag = sd("#modal_add_tag");
    var buttonModalTag = sd("#toggle_modal_tag");
    buttonModalTag.click(function () {
        modalAddTag.addClass("show");
    });
    sd(".toggle_access").click(function (e, elmn) {
        e.preventDefault();
        sd(".register_col_wrap").toggleClass("moved");
    });
    var newTagButton = sd("#action_new_tag");
    newTagButton.click(function (event, thisElement) {
        event.preventDefault();
        var titleTag = sd("#new_title_tag");
        _fn.__ajax({
            url: '/user/create_tag?new_title_tag=' + titleTag.val(),
            method: 'GET',
            success: function (data) {
                var nwData = JSON.parse(data);
                if (nwData["success"]) {
                    sd("#create_post_tag").activeObject[0].disabled = false;
                    sd("#create_post_tag").append("<option>" + nwData["new_data"]['data_post_title'] + "</option>");
                    modalAddTag.removeClass("show");
                    sd("#option_no_tags").remove();
                }
                else {
                }
            }
        });
    });
};
var _fn = {
    __loopOfElements: function (elements, callback) {
        for (var i = 0; i < elements.length; i++) {
            var element = elements[i];
            callback(element);
        }
    },
    __loopOfElementsOnEvent: function (event, elements, callback) {
        this.__loopOfElements(elements, function (element) {
            element.addEventListener(event, function (e) {
                callback(e, this);
            });
        });
    },
    __ajax: function (options) {
        var xhttp = new XMLHttpRequest();
        xhttp.open(options.method, options.url, true);
        xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        if (typeof options.data == 'undefined') {
            xhttp.send();
        }
        else {
            console.log(JSON.stringify(options.data));
            xhttp.send(JSON.stringify(options.data));
        }
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState === xhttp.DONE) {
                if (xhttp.status == 200) {
                    options.success(xhttp.responseText);
                }
                else if (xhttp.readyState == 400) {
                    alert('Error exists number 400');
                }
                else {
                    alert('Internal server error, unknown');
                }
            }
        };
    },
    __hasClass: function (elem, className) {
        return (' ' + elem.className + ' ').indexOf(' ' + className + ' ') > -1;
    }
};
var single_dom = (function () {
    function single_dom(elementCursor) {
        this.activeObject = null;
        this._fn = _fn;
        this.isSingle = false;
        if (elementCursor instanceof HTMLElement) {
            this.activeObject = [elementCursor];
        }
        else if (document.querySelector(elementCursor)) {
            this.activeObject = document.querySelectorAll(elementCursor);
        }
        if (this.exists() && this.activeObject.length == 1) {
            this.isSingle = true;
        }
    }
    single_dom.prototype.hasClass = function (className) {
        if (this.exists()) {
            var checked = false;
            return this._fn.__hasClass(this.activeObject[0], className);
        }
    };
    single_dom.prototype.exists = function () {
        if (this.activeObject) {
            return this.activeObject.length > 0;
        }
        else {
            return false;
        }
    };
    single_dom.prototype.val = function () {
        if (this.exists()) {
            return this.activeObject[0].value;
        }
        else {
            return false;
        }
    };
    single_dom.prototype.append = function (element) {
        if (this.exists()) {
            _fn.__loopOfElements(this.activeObject, function (elm) {
                elm.innerHTML = elm.innerHTML + element;
            });
        }
    };
    single_dom.prototype.remove = function () {
        if (this.exists()) {
            if (this.isSingle) {
                this.activeObject[0].parentNode.removeChild(this.activeObject[0]);
            }
            else {
                this._fn.__loopOfElements(this.activeObject, function (element) {
                    element.parentNode.removeChild(element);
                });
            }
        }
    };
    single_dom.prototype.addClass = function (newClassName) {
        if (this.exists()) {
            this._fn.__loopOfElements(this.activeObject, function (elm) {
                elm.classList.add(newClassName);
            });
        }
    };
    single_dom.prototype.toggleClass = function (toggleClassName) {
        if (this.exists()) {
            this._fn.__loopOfElements(this.activeObject, function (elm) {
                elm.classList.toggle(toggleClassName);
            });
        }
    };
    single_dom.prototype.removeClass = function (removeClassName) {
        if (this.exists()) {
            this._fn.__loopOfElements(this.activeObject, function (elm) {
                elm.classList.toggle(removeClassName);
            });
        }
    };
    single_dom.prototype.click = function (callback) {
        if (this.exists()) {
            this._fn.__loopOfElementsOnEvent("click", this.activeObject, function (e, actualElem) {
                callback(e, actualElem);
            });
        }
    };
    single_dom.prototype.children = function (selector) {
        var _this = this;
        if (selector === void 0) { selector = ''; }
        if (this.activeObject.length === 1) {
            var match = this.activeObject[0];
            var matchs_1 = [];
            this._fn.__loopOfElements(match.childNodes, function (elm) {
                if (elm.nodeType === 1) {
                    matchs_1.push(elm);
                }
            });
            this.activeObject = matchs_1;
        }
        else {
            var match_1 = [];
            for (var i = 0; i < this.activeObject.length; i++) {
                var element = this.activeObject[i];
                this._fn.__loopOfElements(element.childNodes, function (elem) {
                    if (selector != "") {
                        if (_this._fn.__hasClass(elem, selector)) {
                            match_1.push(elem);
                        }
                    }
                    else {
                        if (elem.nodeType === 1) {
                            match_1.push(elem);
                        }
                    }
                });
            }
            this.activeObject = match_1;
        }
        return this;
    };
    return single_dom;
}());
function sd(selector) {
    return new single_dom(selector);
}
