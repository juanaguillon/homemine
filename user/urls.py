from django.urls import path
from . import views

app_name = 'user'

urlpatterns = [
    path('register', views.registerUser, name='register_user'),
    path('login_user', views.loginUser, name='login_user'),

    # URLS usuarios logeads
    path('', views.initialPageUser, name='initial_user' ),

    # Rutas de las publicaciones
    path("create_post", views.createNewPost, name='create_post'),

    # Rutas de metadata de publicaciones ( Etiquetas, categorias, albumes)
    path("create_tag", views.createNewTag, name="create_tag")

]
