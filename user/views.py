from django.shortcuts import render, reverse, redirect
from django.http import JsonResponse
from .models import User, DataPosts, Posts
from django.utils import timezone

# Create your views here.

 
def registerUser(request):
    if request.method == 'POST':
        if request.POST["user_register_name"] == '':
            return False
        if request.POST["user_register_email"] == '':
            return False
        if request.POST["user_register_password"] == '':
            return False
        if request.POST["user_register_rpassword"] == '':
            return False

        if (request.POST["user_register_password"] != request.POST["user_register_rpassword"]):
            return False

        newUserObject = {
            "user_name": request.POST["user_register_name"],
            "user_email": request.POST["user_register_email"],
            "user_password": request.POST["user_register_password"],
            "user_rol": "normal_client"
        }
        existsEmail = User.objects.filter(
            user_email=newUserObject['user_email']).exists()
        if (existsEmail):
            return redirect(reverse("user:register_user") + "?message_register=email_exists")
        else:
            newUser = User(**newUserObject)
            newUser.save()
            return redirect(reverse("user:register_user") + "?message_register=saved")

    else:
        context = dict()
        if request.GET.get("message_register"):
            context["message"] = request.GET["message_register"]
        if request.GET.get("fl"):
            context["fl"] = request.GET["fl"]
        if 'session_id' in request.session:
            context['session_data'] = request.session

        return render(request, 'register_user.html', context)


def loginUser(request):
    userEmail = request.POST["user_login_email"]
    try:
        user = User.objects.get(user_email=userEmail)
        password_passed = request.POST['user_login_password']
        if (user.user_password == password_passed):
            request.session['session_id'] = user.id
            request.session['session_name'] = user.user_name
            request.session['session_email'] = user.user_email
            request.session['session_rol'] = user.user_rol
            return redirect(reverse("user:initial_user"))
        else:
            return redirect(reverse("user:register_user") + "?message_register=email_not_found&fl=1")
    except User.DoesNotExist:
        return redirect(reverse("user:register_user") + "?message_register=email_not_found&fl=1")


####### USUARIOS REGISTRADOS ###############
####### Estas son las rutas de los usuarios registrados. ###########

def initialPageUser(request):

    if not 'session_id' in request.session:
        return redirect(reverse("user:register_user"))
    context = {
        "session": request.session
    }
    return render(request, "loged_users/initial_page.html", context)

## Rutas de publicaciones ##
def createNewPost(request):
    if not 'session_id' in request.session:
        return redirect(reverse("user:register_user"))

    if request.method == "POST":
        context = {
            "images": request.FILES,
            "data": request.POST
        } 
        return render(request, "loged_users/create_post.html", context )
    else:
        theTags = DataPosts.objects.filter(data_post_user=request.session["session_id"])
        context = {
            "session": request.session,
            "tags": theTags
        }
        return render(request, "loged_users/create_post.html", context)

## Rutas de metadata de publcaciones ##

def createNewTag(request):
    if not 'session_id' in request.session:
        return redirect(reverse("user:register_user"))
    
    dataPostContext = {
        "data_post_type": 'tag',
        "data_post_title": request.GET.get('new_title_tag', ""),
        "data_post_date": timezone.now(),
        "data_post_user": request.session["session_id"]
    }
    newDataPost = DataPosts(**dataPostContext)
    newDataPost.save()
    returnData = {
        "success": True,
        "new_data": dataPostContext
    }
    return JsonResponse(returnData)
