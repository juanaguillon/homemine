# Generated by Django 2.2.4 on 2019-08-11 06:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0002_posts'),
    ]

    operations = [
        migrations.CreateModel(
            name='DataPosts',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data_post_title', models.CharField(max_length=245)),
                ('data_post_type', models.CharField(max_length=245)),
                ('data_post_date', models.DateTimeField()),
            ],
        ),
    ]
