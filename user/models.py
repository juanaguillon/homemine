from django.db import models

# Create your models here.

class User(models.Model):
  user_name = models.CharField(max_length=230)
  user_email = models.CharField(max_length=230, unique=True)
  user_password = models.TextField()
  user_rol = models.CharField(max_length=230)

class Posts(models.Model):
  post_date = models.DateField("Creacion de post")
  post_user = models.IntegerField()
  post_title = models.CharField(max_length=245)
  post_desc = models.TextField('Descripcion de post')
  post_image = models.TextField()

class DataPosts(models.Model):
  data_post_title = models.CharField(max_length=245)
  data_post_type = models.CharField(max_length=245)
  data_post_date = models.DateTimeField()
  data_post_user = models.IntegerField(null=True)