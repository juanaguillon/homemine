# Homemine

Una aplicacion creada especificamente para compartir fotografias acerca de nuestro entorno.

## Dependecia NodeJS

Se mantiene una dependecia node js, pero esta, es mas estricta para entorno de desarrollo. Ya que se instala unicamente sass para la compilacion de los estilos de la aplicacion.

### Funcionalidad Javascript

No modifique el archivo custom.js, perdera todas las modificaciones si lo hace
Para agregar nueva funcionalidad javascript, usted debe modificar los archivos .ts.
Para iniciar la compilacion los archivos TS, debe agregar a su linea de comandos `npm run tojs`

### Directorios 'static'

En multiples rutas, encontrara directorios 'static'. Estos directorios se refieren los archivos que el navegador usara para leer archivos css, js e imagenes.

El static principal, se usara en la aplicacion total, y los static internos, para la aplicacion interna directamente.